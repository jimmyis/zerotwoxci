#include "nca.h"

#include "ded_aes.h"
#include "switchkeys.h"
#include "utils.h"
#include "types.h"
#include "cnmt.h"

#include <math.h>
#include <stdlib.h>

#define MAX_WORKBUFFER_SIZE 0x40000000

int key_area_index(nca_header_t* header) {
    int ret;
    if(header->crypto_type1 < header->crypto_type2)
        ret = header->crypto_type2;
    else
        ret = header->crypto_type1;
    
    // I... *think* that 3.x and later crypto actually start with key 2, not 3. And I guess key 1 doesn't exist.
    // In any case, this is what everyone else is doing and it produces correct output.
    if(ret > 0) ret--;
    
    return ret;
}

// It appears the CTR is simply the section's real offset shifted four bits, like so:
// update_ctr taken from hactool © SciresM
void update_ctr(unsigned char *ctr, uint64_t ofs) {
    ofs >>= 4;
    for (unsigned int j = 0; j < 0x8; j++) {
        ctr[0x10-j-1] = (unsigned char)(ofs & 0xFF);
        ofs >>= 8;
    }
}

void decrypt_key_area(nca_header_t* header, char filekeys[4][16]) {
    // We do not write decrypted keys to the header struct,
    // so as to make compatibility with hactool easier.
    int key_idx = key_area_index(header);
    
    aes_ctx_t* aes_ctx = new_aes_ctx(switchkeys.application_key_area[key_idx], 16, AES_MODE_ECB);
    aes_decrypt(aes_ctx, filekeys, header->keys, 16*4);
    free_aes_ctx(aes_ctx);
}

void decrypt_titlekey(nca_header_t* header, char* titlekey) {
    // Find key index
    int key_idx;
    if(header->crypto_type1 < header->crypto_type2)
        key_idx = header->crypto_type2;
    else
        key_idx = header -> crypto_type1;
    
    // Apparently key area key 0 never really existed or something?
    if(key_idx > 0)
        key_idx--;
    
    aes_ctx_t* aes_ctx = new_aes_ctx(switchkeys.titlekeks[key_idx], 16, AES_MODE_ECB);
    aes_decrypt(aes_ctx, titlekey, switchkeys.titlekey, 16);
    free_aes_ctx(aes_ctx);
}


uint8_t nca_content_type_from_cnmt(uint8_t flag) {
    switch(flag) {
        case CNMT_CONTENT_TYPE_META:
            return NCA_CONTENT_TYPE_META;
        case CNMT_CONTENT_TYPE_PROGRAM:
            return NCA_CONTENT_TYPE_PROGRAM;
        case CNMT_CONTENT_TYPE_DATA:
            return NCA_CONTENT_TYPE_DATA;
        case CNMT_CONTENT_TYPE_CONTROL:
            return NCA_CONTENT_TYPE_CONTROL;
        case CNMT_CONTENT_TYPE_MANUAL:
            return NCA_CONTENT_TYPE_MANUAL;
        case CNMT_CONTENT_TYPE_LEGALINFO:
            return NCA_CONTENT_TYPE_MANUAL; // XXX: Blind guess
        case CNMT_CONTENT_TYPE_GAME_UPDATE:
            return NCA_CONTENT_TYPE_DATA; // XXX: Blind guess
    }
}

bool nca_has_rightsid(nca_header_t* header) {
    static char zeroes[16]; // Implicitly zero-filled because static
    if( memcmp( zeroes, header->rights_id, 16) != 0 )
        return true;
  
    return false;
}

void nca_init_header(nca_header_t* hd, nca_header_t* old_hd) {
    memset(hd, 0, sizeof(nca_header_t));
    strncpy(hd->magic, "NCA3", 4);
    
    if( old_hd != NULL ) {
        
        hd->location = old_hd->location;
        hd->content_type = old_hd->content_type;
        hd->crypto_type1 = old_hd->crypto_type1;
        hd->key_index = old_hd->key_index;
        hd->titleid = old_hd->titleid;
        hd->sdk_version = old_hd->sdk_version;
        hd->crypto_type2 = old_hd->crypto_type2;
        memcpy(hd->rights_id, old_hd->rights_id, 16);
        memcpy(hd->keys, old_hd->keys, 4*16);
    }
    else {
        hd->location = NCA_LOCATION_CONSOLE;
        hd->content_type = NCA_CONTENT_TYPE_PROGRAM;
        hd->crypto_type1 = NCA_CRYPTO_TYPE1_PRE_3;
        hd->key_index = 0;
        // TODO: title ID?
        hd->sdk_version = 0x1020000; // Value from most launch titles
        hd->crypto_type2 = 0;
    }
}

void nca_decrypt_header(nca_header_t* out, char* in) {
    aes_ctx_t* aes_ctx = new_aes_ctx(switchkeys.header, sizeof(switchkeys.header), AES_MODE_XTS);
    aes_xts_decrypt(aes_ctx, out, in, sizeof(nca_header_t), 0, 0x200);
    free_aes_ctx(aes_ctx);
}

void nca_encrypt_header(char* out, nca_header_t* in) {
    aes_ctx_t* aes_ctx = new_aes_ctx(switchkeys.header, sizeof(switchkeys.header), AES_MODE_XTS);
    aes_xts_encrypt(aes_ctx, out, in, sizeof(nca_header_t), 0, 0x200);
    free_aes_ctx(aes_ctx);
}

uint64_t nca_offsetof_section(nca_header_t* header, unsigned int section) {
    return header->section_table[section].media_offset * 0x200ULL;
}

aes_ctx_t* make_section_crypto_ctx(nca_header_t* header, unsigned int section) {
    char filekeys[4][16];
    char iv[16];
    
    decrypt_key_area(header, filekeys);
    
    memset(iv, 0x0, 16);

    // Read portion of CTR from section header
    // Byte swapping because good ol' LSB
    for(int i=0; i<8; i++) {
        int j=7-i;
        iv[i] = header->section_headers[section].section_ctr[j];
    }
    
    aes_ctx_t* ret;
    if( nca_has_rightsid(header) ) {
        char decrypted_titlekey[16];
        decrypt_titlekey(header, decrypted_titlekey);
        ret = new_aes_ctx(decrypted_titlekey, 16, AES_MODE_CTR);
    }
    else
        // Keys 0, 1, and 3 are always the same and usually zeroes.
        // Also, hactool hardcodes to 2. Hmm.
        ret = new_aes_ctx(filekeys[2], 16, AES_MODE_CTR);
    
    update_ctr(iv, header->section_table[section].media_offset*0x200);
    aes_setiv(ret, iv, 16);
    
    return ret;
}

void nca_decrypt_section(nca_header_t* header, unsigned int section, char* out, char* in, uint64_t size) {
    aes_ctx_t* ctx = make_section_crypto_ctx(header, section);
    aes_decrypt(ctx, out, in, size);
    free_aes_ctx(ctx);
}

void nca_encrypt_section(nca_header_t* header, unsigned int section, char* out, char* in, uint64_t size) {
    aes_ctx_t* ctx = make_section_crypto_ctx(header, section);
    aes_encrypt(ctx, out, in, size);
    free_aes_ctx(ctx);
}

// XXX So many arguments! Is there a cleaner way?
void copy_with_crypto(aes_ctx_t* aes_ctx, uint64_t size,
        baecb_write out, void* outcbdata, uint64_t out_offset,
        baecb_read in, void* incbdata, uint64_t in_offset,
        bool encrypt) {
    char inbuf[0x200];
    char outbuf[0x200];
    
    for(uint64_t i=0; i<size; i+= 0x200) {
        if( i+0x200 < size )
            memset(inbuf, 0, 0x200);
        (*in)(inbuf, i + in_offset, 0x200, incbdata);
        if(encrypt)
            aes_encrypt(aes_ctx, &outbuf, &inbuf, 0x200);
        else
            aes_decrypt(aes_ctx, &outbuf, &inbuf, 0x200);
        (*out)(outbuf, i + out_offset, 0x200, outcbdata);
    }
}

void nca_extract_section(nca_header_t* header, unsigned int section,
        baecb_write out, void* outcbdata,
        baecb_read in, void* incbdata) {
    
    aes_ctx_t* aes_ctx = make_section_crypto_ctx(header, section);
    copy_with_crypto(aes_ctx, nca_sizeof_section(header, section),
            out, outcbdata, 0,
            in, incbdata, nca_offsetof_section(header, section),
            false);
    free_aes_ctx(aes_ctx);
}

void nca_replace_section(nca_header_t* header, unsigned int section,
        baecb_write out, void* outcbdata,
        baecb_read in, void* incbdata) {
    
    aes_ctx_t* aes_ctx = make_section_crypto_ctx(header, section);
    copy_with_crypto(aes_ctx, nca_sizeof_section(header, section),
            out, outcbdata, nca_offsetof_section(header, section),
            in, incbdata, 0,
            true);
    free_aes_ctx(aes_ctx);
}

void nca_inject_pfs0(nca_header_t* header, unsigned int section,
        baecb_write out, void* outcbdata,
        baecb_read in, void* incbdata, uint64_t fs_size) {
    nca_section_header_t* sec_hd = &header->section_headers[section];
    
    // Collect some starting values
    int blocksize = sec_hd->pfs0.blocksize;
    if(blocksize == 0)
        // TODO: Intelligently choose blocksize based on... dunno, some criteria
        blocksize = 0x1000;
    // One 32 byte hash per 0x1000 bytes
    unsigned int hashtable_size = 32 * ceil(fs_size / (float) blocksize);
    
    // Fill in parts of the section header/superblock we already know
    sec_hd->partition_type = NCA_PARTITION_TYPE_PFS0;
    sec_hd->fstype = NCA_FSTYPE_PFS0;
    if( sec_hd->cryptotype == 0 )
        sec_hd->cryptotype = NCA_SECTION_CRYPTO_TYPE_CTR;
    
    sec_hd->pfs0.blocksize = blocksize;
    sec_hd->pfs0.unknown_0 = 0x2;
    sec_hd->pfs0.hashtable_offset = 0x0;
    sec_hd->pfs0.hashtable_size = hashtable_size;
    if( sec_hd->pfs0.header_offset == 0 )
        sec_hd->pfs0.header_offset = hashtable_size;
    sec_hd->pfs0.fs_size = fs_size;
    
    // Calculate the penultimate section size
    uint64_t section_mediasize = ceil( (fs_size + hashtable_size) / (float) 0x200 );
    uint64_t section_bytesize = section_mediasize * 0x200;
    header->section_table[section].media_end = header->section_table[section].media_offset + section_mediasize;
    
    if(hashtable_size > MAX_WORKBUFFER_SIZE)
        //bail("Absurdly large file! Are you SURE that's correct?");
        // TODO: Return error
        return;
    
    // Allocate input and output buffers
    char* inbuf;
    char* outbuf;
    if(section_bytesize > MAX_WORKBUFFER_SIZE) {
        inbuf = malloc(MAX_WORKBUFFER_SIZE);
        outbuf = malloc(MAX_WORKBUFFER_SIZE);
    }
    else {
        inbuf = malloc(section_bytesize);
        outbuf = malloc(section_bytesize);
    }
    
    // Build and write hash table
    char* hashtable = malloc(hashtable_size);
    int i=0;
    while((i+1)*32 < hashtable_size) {
        uint64_t readsize = (*in)(inbuf, i*blocksize, blocksize, incbdata);
        if( readsize != blocksize )
            // bail("Failed to read full block from input");
            // TODO: Return error
        
        hash_buf_256(inbuf, hashtable + i*32, blocksize);
        i++;
    }
    // Last hash in the table
    unsigned int last_block_size = fs_size - i*blocksize;
    uint64_t readsize = (*in)(inbuf, i*blocksize, last_block_size, incbdata);
    hash_buf_256(inbuf, hashtable + i*32, readsize);
    
    // Master hash
    hash_buf_256(hashtable, sec_hd->pfs0.master_hash, hashtable_size);
    
    aes_ctx_t* aes_ctx;
    if(sec_hd->cryptotype == NCA_SECTION_CRYPTO_TYPE_PLAINTEXT)
        aes_ctx = NULL;
    else
        aes_ctx = make_section_crypto_ctx(header, section);
    
    // Encrypt and write hashtable
    if(aes_ctx == NULL)
        memcpy(outbuf, hashtable, hashtable_size);
    else
        aes_encrypt(aes_ctx, outbuf, hashtable, hashtable_size);
    
    free(hashtable);
    
    (*out)(outbuf, nca_offsetof_section(header, section), hashtable_size, outcbdata);
    
    // Encrypt and write the PFS0 itself
    uint64_t pos = 0;
    uint64_t out_offset = nca_offsetof_section(header, section) + hashtable_size;
    while(pos + MAX_WORKBUFFER_SIZE < section_bytesize) {
        (*in)(inbuf, pos, MAX_WORKBUFFER_SIZE, incbdata);
        aes_encrypt(aes_ctx, outbuf, inbuf, MAX_WORKBUFFER_SIZE);
        (*out)(outbuf, pos + out_offset, MAX_WORKBUFFER_SIZE, outcbdata);
        pos += MAX_WORKBUFFER_SIZE;
    }
    // Last chunk
    uint64_t remaining = section_bytesize - pos;
    (*in)(inbuf, pos, remaining, incbdata);
    int padding = section_bytesize - fs_size;
    memset(inbuf + fs_size, 0, padding);
    aes_encrypt(aes_ctx, outbuf, inbuf, remaining);
    (*out)(outbuf, pos + out_offset, remaining, outcbdata);
    
    // Header hash
    hash_buf_256(sec_hd, header->hashes[section], sizeof(nca_section_header_t));
    
    free(inbuf);
    free(outbuf);
}

uint64_t nca_sizeof_section(nca_header_t* header, unsigned int section) {
    return header->section_table[section].media_end*0x200ULL - header->section_table[section].media_offset*0x200ULL;
}
