#pragma once

#include "baecb.h"

#include <stdint.h>

// Make sha256 hash out of file
void hash_file_256(char* hash, baecb_read in, void* cbdata);
// Make sha256 hash out of buffer
void hash_buf_256(void* buf, char* hash, unsigned int buf_size);
