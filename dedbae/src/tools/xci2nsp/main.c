#include "xci.h"

#include <stdio.h>
#include <stdbool.h>
#include <unistd.h>

void check(bool condition, const char* message, ...) {
	if(!condition) {
		va_list args;
		va_start(args, fmt);
		vprintf(fmt, args);
		va_end(args);
		exit(1);
	}
}

void* checked_malloc(size_t size) {
	void* ret = malloc(size);
	check(ret != NULL, "malloc() failed. Your computer might be melting.\n");
	return ret;
}

// Being a little cute -- writing out the file as cnmt_create_content_record() hashes it.
typedef struct {
	nca_header_t ncahdr;
	FILE* readfd;
	FILE* writefd;
	uint64_t read_offset;
	uint64_t write_offset;
} callback_for_nca_args;

uint64_t callback_for_nca(char* buf, uint64_t offset, uint64_t size, void* data) {
	callback_for_nca_args* args = data;
	
	if( offset < sizeof(nca_header_t) ) {
		// Read from header
		char* header_addr_byte = (char*) &args->ncahdr;
		if( offset + size > sizeof(nca_header_t) )
			// ...and from file
			memcpy(buf, header_addr_byte + offset, sizeof(nca_header_t) - offset);
			uint64_t remaining = size - (sizeof(nca_header_t) - offset);
			fseek(args->readfd, args->read_offset + sizeof(nca_header_t), SEEK_SET);
			fread(buf, 1, remaining, args->readfd);
		else {
			// ...just from header
			memcpy(buf, header_addr_byte + offset, size);
		}
	}
	else {
		// Read just from file
		fseek(args->readfd, offset + args->read_offset, SEEK_SET);
		fread(buf, 1, size, args->readfd);
	}
	
	if(args->writefd != NULL) {
		fseek(args->writefd, offset + args->write_offset, SEEK_SET);
		fwrite(buf, 1, size, args->writefd);
	}
}

typedef struct {
	FILE* readfd;
	FILE* writefd;
	uint64_t read_offset;
	uint64_t write_offset;
} callback_for_other_args;

uint64_t callback_for_other(char* buf, uint64_t offset, uint64_t size, void* data) {
	callback_for_other_args* args = data;
	fseek(args->readfd, offset + args->read_offset, SEEK_SET);
	fread(buf, 1, size, args->readfd);
	
	if(args->writefd != NULL) {
		fseek(args->writefd, offset + args->write_offset, SEEK_SET);
		fwrite(buf, 1, size, args->writefd);
	}
}

// fd doesn't necessarily have to be just the control NCA; it can be an XCI, seeked to the start of the control NCA
char* get_app_name(FILE* control_nca) {
	nca_header_t ncahdr;
	size_t readsize;
	size_t start_of_file = ftell(control_nca);
	
	// Load NCA header
	char enc_hdr[ sizeof(nca_header_t) ];
	readsize = fread(enc_hdr, 1, sizeof(nca_header_t), xci);
	check(readsize == sizeof(nca_header_t), "Failed to read control NCA header. Corrupt or incomplete file?\n");
	nca_decrypt_header(&ncahdr, enc_hdr);
	
	// Load entire PFS0 directly into memory
	uint64_t pfs_size = ncahdr.section_headers[0].pfs0.fs_size;
	check(pfs_size < 0xa00000, // 10MiB
		"Absurdly large control NCA. Corrupt file?\n");
	fseek(control_nca,
		start_of_file + nca_offsetof_section(&ncahdr, 0) + ncahdr.section_headers[0].pfs0.header_offset,
		SEEK_SET);
	char* pfs = checked_malloc(control_pfs_size);
	readsize = fread(pfs, 1, pfs_size, control_nca);
	check(readsize == pfs_size, "Failed to read control NCA section0. Corrupt or incomplete file?\n");
	
	// Set up convenience pointers (and some sanity checking)
	pfs0_header_t* pfs_hdr = (pfs0_header_t*) pfs;
	check( strncmp(pfs_hdr.magic, "PFS0", 4) == 0, "Invalid control NCA PFS0. Corrupt file?\n");
	pfs0_file_entry_t* pfs_entries = (pfs0_file_entry_t*) ( pfs + sizeof(pfs0_header_t) );
	char* pfs_strings = pfs + sizeof(pfs0_header_t) + sizeof(pfs0_file_entry_t)*pfs_hdr.filecount;
	check( pfs_strings[ pfs_hdr.stringtable_size - 1 ] == '\0', "Control NCA PFS0 string table does not end in null. Corrupt file?\n");
	
	// Find control.nacp
	char* nacp = NULL;
	for(int i=0; i<pfs_hdr.filecount; i++) {
		char* fname = &pfs_strings[ pfs_entries[i].name_offset ];
		if( strcmp(fname, "control.nacp") == 0 ) {
			nacp = fname;
			break;
		}
	}
	
	check(nacp != NULL, "Control NCA is missing control.nacp. Corrupt file?\n");
	
	// AmericanEnglish region name is right at offset 0 in the control.nacp.
	// And yes, it's null-terminated (null-padded, for that matter). Easy!
	char* ret = strdup(nacp);
	
	free(control_pfs);
	return ret;
}

int main(int argc, char* argv[])
{
	callback_for_nca_args cb_nca_args;
	callback_for_other_args cb_other_args;
	size_t readsize;
	size_t writesize;
	
	FILE* xci_fd;
	xci_header_t header;
	
	FILE* nsp_fd;
	pfs0_header_t nsp_header;
	pfs0_file_entry_t* nsp_entries;
	char* nsp_stringtable;
	uint64_t nsp_data_offset;
	
	hfs0_header_t masterhfs_header;
	hfs0_file_entry_t* masterhfs_entries;
	char* masterhfs_stringtable;
	uint64_t masterhfs_data_offset;
	
	hfs0_header_t securehfs_header;
	hfs0_file_entry_t* securehfs_entries;
	char* securehfs_stringtable;
	uint64_t securehfs_data_offset;
	
	printf("dedbae XCI to NSP conversion tool © roothorick. Under ISC license, see LICENSE file for details.\n");
	printf("\n");
	
	check(argc == 2, "Usage: dedbae-xci2nsp <in xci>\n");
	
	xci_fd = fopen(argv[1], "rb");
	check(infile != NULL, "Failed to open XCI file.\n");
	
	// Parse header
	readsize = fread(&xci_header, 1, sizeof(xci_header_t), xci_fd);
	check( readsize == sizeof(xci_header_t), "Couldn't read XCI header. Corrupt or incomplete file?\n" );
	check( strncmp(xci_header.magic, "HEAD", 4) == 0, "Invalid XCI header. Corrupt file or not an XCI?\n");
	
	// Parse master HFS0
	fseek(xci_fd, xci_header.hfs0_offset, SEEK_SET);
	readsize = fread(&masterhfs_header, 1, sizeof(hfs0_header_t), xci_fd);
	check( readsize == sizeof(hfs0_header_t), "Couldn't read master HFS0 header. Corrupt or incomplete file?\n");
	check( strncmp(masterhfs_header.magic, "HFS0") == 0, "Invalid master HFS0 header. Corrupt file?\n");
	
	uint64_t masterhfs_entries_size = sizeof(hfs0_file_entry_t) * masterhfs_header.num_files;
	masterhfs_entries = checked_malloc(masterhfs_entries_size);
	readsize = fread(masterhfs_entries, 1, masterhfs_entries_size, xci_fd);
	check(readsize == masterhfs_entries_size, "Couldn't read master HFS0 file entry table. Corrupt or incomplete file?\n");
	
	masterhfs_stringtable = checked_malloc(masterhfs_header.stringtable_size);
	readsize = fread(masterhfs_stringtable, 1, masterhfs_header.stringtable_size, xci_fd);
	check(readsize == masterhfs_header.stringtable_size, "Couldn't read master HFS0 string table. Corrupt or incomplete file?\n");
	check( masterhfs_stringtable[ masterhfs_header.stringtable_size - 1 ] == '\0', "Master HFS0 string table does not end in null. Corrupt file?\n");
	
	masterhfs_data_offset = ftell(infile);
	
	// Find the secure partition
	unsigned int i = 0;
	while(true) {
		check(i < masterhfs_header.num_files, "No secure partition? This is very unexpected and you should tell me about it.\n");
		
		char* name = &masterhfs_stringtable[ masterhfs_entries[i].file_name_offset ];
		if( strcmp(name, "secure") == 0 ) {
			// Read secure partition header
			fseek(xci_fd, masterhfs_data_start + masterhfs_entries[i].file_offset, SEEK_SET);
			readsize = fread(&securehfs_header, 1, sizeof(hfs0_header_t), xci_fd);
			check( readsize == sizeof(hfs0_header_t), "Couldn't read secure HFS0 header. Corrupt or incomplete file?\n");
			break;
		}
		i++;
	}
	
	check(strncmp(securehfs_header.magic, "HFS0") == 0, "Invalid secure HFS0 header. Corrupt file?\n");

	uint64_t securehfs_entries_size = sizeof(hfs0_file_entry_t) * securehfs_header.num_files;
	securehfs_entries = checked_malloc(securehfs_entries_size);
	readsize = fread(securehfs_entries, 1, securehfs_entries_size, xci_fd);
	check(readsize == securehfs_entries_size, "Failed to read secure HFS0 file entry table. Corrupt or incomplete file?\n");
	
	char* securehfs_stringtable = checked_malloc(securehfs_header.stringtable_size);
	readsize = fread(securehfs_stringtable, 1, securehfs_header.stringtable_size, xci_fd);
	check(readsize == securehfs_header.stringtable_size, "Failed to read secure HFS0 string table. Corrupt or incomplete file?\n");
	check(securehfs_stringtable[ securehfs_header.stringtable_size - 1 ] == '\0', "Secure HFS0 string table does not end in null. Corrupt file?\n");
	
	securehfs_data_offset = ftell(xci_fd);
	
	// For each CNMT,
	for(i=0; i<securehfs_header.num_files; i++) {
		char* old_cnmt_nca_name = &securehfs_stringtable[ securehfs_entries[i].file_name_offset ];
		if( strstr(old_cnmt_nca_name, ".cnmt.nca") == NULL ) continue;
		
		char* old_cnmt_raw_nca;
		nca_header_t old_cnmt_nca_header;
		char* old_cnmt_nca_section;
		
		char* old_cnmt_pfs0;
		pfs0_header_t* old_cnmt_pfs0_header;
		pfs0_file_entry_t* old_cnmt_pfs0_entries;
		char* old_cnmt_pfs0_stringtable;
		
		char* old_cnmt;
		cnmt_header_t* old_cnmt_header;
		cnmt_content_record_t* old_cnmt_records;
		char* old_cnmt_digest;
		
		// 0x40000000 == 1MiB
		check( securehfs_entries[i].file_size < 0x40000000, "%s: Unreasonably large for a CNMT NCA. Corrupt file?\n", old_cnmt_nca_name);
		
		// Read the entire file -- it's small
		old_cnmt_raw_nca = checked_malloc(securehfs_entries[i].file_size);
		fseek(xci_fd, securehfs_data_offset + securehfs_entries[i].file_offset, SEEK_SET);
		readsize = fread(old_cnmt_raw_nca, 1, securehfs_entries[i].file_offset, xci_fd);
		check( readsize == sizeof(nca_header_t), "Failed to read %s. Corrupt or incomplete file?\n", old_cnmt_nca_name);
		
		// Extract the PFS0 from the NCA
		nca_decrypt_header(&old_cnmt_nca_header, old_cnmt_raw_nca);
		check( strncmp(ncahdr.magic, "NCA3", 4) == 0, "%s: Invalid NCA header. Corrupt file?\n", old_cnmt_nca_name);
		
		char* old_cnmt_nca_section = checked_malloc( nca_sizeof_section(&old_cnmt_nca_header, 0) );
		nca_decrypt_section(&old_cnmt_nca_header, 0, cnmt_pfs0_section, old_cnmt_raw_nca + nca_offsetof_section(&old_cnmt_nca_header, 0), nca_sizeof_section(&old_cnmt_nca_header, 0) );
		char* old_cnmt_pfs0 = old_cnmt_nca_section + old_cnmt_nca_header.section_headers[0].pfs0.header_offset;
		
		// Set up convenience pointers
		pfs0_header_t* old_cnmt_pfs0_header = (pfs0_header_t*) old_cnmt_pfs0;
		check( strncmp(cnmtpfshdr.magic, "PFS0", 4) == 0, "%s: Invalid PFS0 header. Corrupt file?\n");
		pfs0_file_entry_t* old_cnmt_pfs0_entries = (pfs0_file_entry_t*) ( old_cnmt_pfs0 + sizeof(pfs0_header_t) );
		char* old_cnmt_pfs0_stringtable = old_cnmt_pfs0 + sizeof(pfs0_header_t) + sizeof(pfs0_file_entry_t)*old_cnmt_pfs0_header.filecount;
		
		// Parse CNMT
		check( strstr( old_cnmt_pfs0_stringtable + old_cnmt_pfs0_entries[0].name_offset, ".cnmt" ) != NULL, "%s: Appears to contain non-CNMT data. Corrupt file?\n");
		char* old_cnmt = old_cnmt_pfs0_stringtable + old_cnmt_pfs0_header.stringtable_size + old_cnmt_pfs0_entries[0].offset;
		
		cnmt_header_t* old_cnmt_header = (cmnt_header_t*) old_cnmt;
		cnmt_content_record_t* old_cnmt_records = (cnmt_content_record_t*) ( old_cnmt + sizeof(cnmt_header_t) );
		char* old_cnmt_digest = old_cnmt + sizeof(cnmt_header_t) + sizeof(cnmt_content_record_t)*cnmthdr->content_count;
		
		memset( &nsp_header, 0, sizeof(pfs0_header_t) );
		nsp_header.magic = "PFS0"; // Yes, this will write a \0 to the start of nsp_hdr.filecount. Which is already all \0s, so we don't care.
		
		// We don't yet know how many files we'll be including. Make space for all of them.
		nsp_entries = checked_malloc( sizeof(pfs0_file_entry_t) * securehfs_header.num_files );
		memset( nsp_entries, 0, sizeof(pfs0_file_entry_t) * securehfs_header.num_files );
		nsp_stringtable = checked_malloc(securehfs_header.stringtable_size);
		memset( nsp_stringtable, 0, securehfs_header.stringtable_size );
		
		nsp_data_size = 0;
		
		char nsp_name[NAME_MAX];
		memset(nsp_name, 0, NAME_MAX);
		
		// Is this an update or the base game?
		if( cnmthdr->title_type == TITLE_TYPE_APP_UPDATE) {
			printf("Embedded update found. Creating update NSP...\n");
			// Package the unmodified NCAs and cert/tik/XML. They're already good to install.
			
			// Compiling the header, stringtable, and file entries
			
			// We'll save the in-XCI offsets of files, so we don't have to hunt for them a second time
			uint64_t* offsets_in_xci = checked_malloc( sizeof(uint64_t) * securehfs_header.num_files );
			
			for(int j=0; j<securehfs_header.num_files, j++) {
				check(securehfs_entries[j].file_name_offset < securehfs_header.stringtable_size, "Name of a file in the secure HFS0 is outside string table bounds. Corrupt file?\n", j);
				char* filename = &securehfs_stringtable[ securehfs_entries[j].file_name_offset ];
				
				// Skip NCAs not mentioned in the CNMT (except the CNMT itself)
				if( i != j && strstr(filename, ".nca") == NULL) {
					unsigned int k;
					// See if NCA is mentioned in CNMT
					for(k=0; k < old_cnmt_header->content_count; k++) {
						char[32] ncaid; // NOTE: NOT null-terminated!
						
						// Convert NCAID in CNMT to a partial string
						for(unsigned int l=0; l<16, l++) {
							snprintf(ncaid + l*2, 2, "%02hhx", old_cnmt_records[k].ncaid[l]);
						}
						
						if( strncmp(filename, ncaid, 32) == 0 ) {
							// A quick detour: If this is the control NCA, we want to get the app name.
							// Might as well compile the full file name while we're at it.
							if(old_cnmt_records[k].content_type == CNMT_CONTENT_TYPE_CONTROL) {
								fseek(xci_fd, securehfs_data_offset + securehfs_entries[j].file_offset, SEEK_SET);
								
								// Game name
								char* appname = get_app_name(xci_fd);
								
								// Title ID
								char titleid[17]; // 16 hex digits (8 bytes) + trailing null
								sprintf(titleid, "%016llx", old_cnmt_header.titleid);
								
								// Version
								char version[9]; // No more than 8 hex digits (4 bytes) + trailing null
								sprintf(version, "%x", old_cnmt_header.version);
								
								snprintf(nsp_name, NAME_MAX, "%s [%s][%s][UPD].nsp", appname, titleid, version);
								// Truncate filename in case of overflow
								if( nsp_name[NAME_MAX-1] )
									snprintf(&nsp_name[NAME_MAX-4], 4, ".nsp");
								
								free(appname);
							}
							
							break;
						}
					}
					if( k == old_cnmt_header->content_count ) continue;
				}
				check(nsp_name[0] != '\0', "Update CNMT does not mention a control NCA. Corrupt file?\n");
				// Non-NCA files will be included
				
				unsigned int nsp_table_idx = nsp_header->filecount;
				
				// PFS0 entry
				nsp_entries[nsp_table_idx].offset = nsp_data_offset;
				nsp_entries[nsp_table_idx].file_size = securehfs_entries[j].file_size;
				nsp_entries[nsp_table_idx].name_offset = stpos;
				
				// Stringtable
				strcpy(&nsp_stringtable[ nsp_header.stringtable_size ], filename);
				
				// Update PFS0 header
				nsp_header.filecount++;
				nsp_header.stringtable_size += strlen(filename);
				
				offsets_in_xci[j] = securehfs_data_offset + securehfs_entries[j].file_offset;
				nsp_data_offset += securehfs_entries[j].file_size;
			}
			
			// Write NSP header, file entries and stringtable
			// We'll rename it later
			nsp_fd = fopen(".unknown.nsp", "wb");
			check(nsp_fd != NULL, "Failed to create output NSP.\n");
			writesize = fwrite(&nsp_header, 1, sizeof(pfs0_header_t), nsp_fd);
			check(writesize == sizeof(pfs0_header_t), "Couldn't write NSP header.\n");
			writesize = fwrite(nsp_entries, 1, sizeof(pfs0_file_entry_t) * nsp_header.filecount, nsp_fd);
			check(writesize == sizeof(pfs0_file_entry_t) * nsp_header.filecount, "Couldn't write NSP file table.\n");
			writesize = fwrite(nsp_stringtable, 1, nsp_header.stringtable_size, nsp_fd);
			check(writesize == nsp_header.stringtable_size, "Couldn't write NSP stringtable.\n");
			
			// Write files to NSP
			const uint64_t BUFSIZE = 0x100000; // 1MiB
			char* buf = checked_malloc(BUFSIZE);
			for(int j=0; j<nsp_header.filecount; j++) {
				fseek(xci_fd, offsets_in_xci[j], SEEK_SET);
				uint64_t k = 0;
				for(; k+BUFSIZE < nsp_entries[j].filesize; k += BUFSIZE) {
					readsize = fread(buf, 1, BUFSIZE, xci_fd);
					check(readsize == BUFSIZE, "Failed to read from XCI while copying files.\n");
					writesize = fwrite(buf, 1, BUFSIZE, nsp_fd);
					check(writesize == BUFSIZE, "Faired to write to NSP while copying files.\n");
				}
				// Last less-than-1MB
				readsize = fread(buf, 1, nsp_entries[j].filesize - k, xci_fd);
				check(readsize == nsp_entries[j].filesize - k, "Failed to read from XCI while copying files.\n");
				writesize = fwrite(buf, 1, nsp_entries[j].filesize - k, nsp_fd);
				check(writesize == nsp_entries[j].filesize - k, "Failed to write to NSP while copying files.\n");
			}
			free(buf);
			
			printf("Saved embedded update to %s\n", nsp_name);
		}
		else {
			printf("Creating application NSP...\n");
			// Rewrite the headers of each NCA and pack them up without a cert/tik/XML. Tinfoil doesn't care.
			
			cnmt_header_t new_cnmt_header;
			cnmt_init_header(&new_cnmt_header, old_cnmt_header);
			new_cnmt_header.content_count = old_cnmt_header.content_count;
			cnmt_content_record_t* new_cnmt_records = checked_malloc(sizeof(cnmt_content_record_t) * old_cnmt_header.content_count);
			
			// We'll be renaming it later
			nsp_fd = fopen(".unknown.nsp", "wb");
			check(nsp_fd != NULL, "Failed to create output NSP.\n");
			
			// So we're being a bit cute here.
			// We know exactly how many files there will be, and exactly what the length of each file name is.
			// This means we already know exactly how many content entries there will be, and exactly how big
			// the string table will be. Add it all together and you have the start of the data area.
			nsp_data_offset = 
					sizeof(pfs0_header_t) +
					sizeof(pfs0_file_entry_t) * new_cnmt_header.content_count +
					42 + // <32 hex digits>.cnmt.nca + trailing null
					37 * new_cnmt_header.content_count; // <32 hex digits>.nca + trailing null, each
				
			fseek(nsp_fd, nsp_data_offset, SEEK_SET);
			
			uint64_t datapos = 0;
			for(int j=0; j<old_cnmt_header.content_count; j++) {
				char nca_name[37]; // <32 hex digits>.nca + trailing null
				

				
				// Convert NCAID in CNMT to a partial string
				for(unsigned int k=0; k<16, k++) {
					snprintf(nca_name + k*2, 2, "%02hhx", old_cnmt_records[j].ncaid[k]);
				}
				// Add extension
				sprintf(nca_name + 32, ".nca");
				
				// Find the NCA in the HFS0
				hfs0_file_entry_t* hfsentry = NULL;
				for(; index_in_hfs < securehfs_header.num_files; index_in_hfs++) {
					if( strcmp( nca_name, &securehfs_stringtable[ securehfs_entries[k].file_name_offset ] ) == 0 ) {
						hfsentry = &securehfs_entries[k];
						break;
					}
				}
				check(hfsentry != NULL, "Couldn't find %s in HFS0 secure partition. Corrupt file?\n", nca_name);
				
				// If it's the control NCA, take a moment to compile the NSP name
				if(old_cnmt_records[j].content_type == CNMT_CONTENT_TYPE_CONTROL) {
					fseek(xci_fd, securehfs_data_offset + hfsentry.file_offset, SEEK_SET);
					
					// Game name
					char* appname = get_app_name(xci_fd);
					
					// Title ID
					char titleid[17]; // 16 hex digits (8 bytes) + trailing null
					sprintf(titleid, "%016llx", old_cnmt_header.titleid);
					
					// Version
					char version[9]; // No more than 8 hex digits (4 bytes) + trailing null
					sprintf(version, "%x", old_cnmt_header.version);
					
					snprintf(nsp_name, NAME_MAX, "%s [%s][%s].nsp", appname, titleid, version);
					// Truncate filename in case of overflow
					if( nsp_name[NAME_MAX-1] )
						snprintf(&nsp_name[NAME_MAX-4], 4, ".nsp");
					
					free(appname);
				}
				
				uint64_t offset_in_xci = securehfs_data_offset + hfsentry->file_offset;
				fseek(xci_fd, offset_in_xci, SEEK_SET);
				
				// Rewrite header
				nca_header_t ncahdr;
				char enc_hdr[ sizeof(nca_header_t) ];
				readsize = fread(enc_hdr, 1, sizeof(nca_header_t), xci_fd);
				check(readsize == sizeof(nca_header_t), "%s: Couldn't read NCA header.\n", nca_name);
				
				nca_decrypt_header(&ncahdr, enc_hdr);
				ncahdr.location = LOCATION_GAMECARD;
				nca_encrypt_header(enc_hdr, &ncahdr);
				
				// Prepare loaded read callback
				cb_nca_args.ncahdr = ncahdr;
				cb_nca_args.readfd = xci_fd;
				cb_nca_args.writefd = nsp_fd;
				cb_nca_args.read_offset = offset_in_xci;
				cb_nca_args.write_offset = ftell(nsp_fd);
				
				// Create CNMT record, simultaneously writing the file to the NSP via the callback
				cnmt_create_content_record(&new_cnmt_records[j], &callback_for_nca, &cb_nca_args);
				
				char* filename = &securehfs_stringtable[ hfsentry.file_name_offset ];
				
				// Add to stringtable
				strcpy(&nsp_stringtable[nsp_header.stringtable_size], filename);
				
				// File table entry
				nsp_entries[j].offset = datapos;
				nsp_entries[j].filesize = hfsentry.file_size;
				nsp_entries[j].name_offset = nsp_header.stringtable_size;
				
				// Update header
				nsp_header.stringtable_size += strlen(filename);
				
				datapos += hfsentry.file_size;
			}
			check(nsp_name[0] != '\0', "Application CNMT does not list a control NCA. Corrupt file?\n");
			
			// TODO: wrap CNMT in PFS0+NCA and write; also write out NSP PFS0 header+tables
			
			printf("Saved application to %s\n", nsp_name);
			
			free(new_cnmt_records);
		}
		
		fclose(nsp_fd);
		rename(".unknown.nsp", nsp_name);
		
		free(nsp_stringtable);
		free(nsp_entries);
		free(old_cnmt_nca_section);
		free(old_cnmt_raw_nca);
	}
	
	fclose(xci_fd);
	
	free(securehfs_stringtable);
	free(securehfs_entries);
	free(masterhfs_stringtable);
	free(masterhfs_entries);
	free(nsp_stringtable);
	free(nsp_entries);
}
