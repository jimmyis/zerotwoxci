#include "actions.h"

#include "xci.h"
#include "hfs0.h"
#include "utils.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/sendfile.h>

void act_extractxci(char* file) {
	FILE* fd = fopen(file, "rb");
	
	xci_header_t header;
	fread(&header, 1, sizeof(xci_header_t), fd);

	hfs0_header_t masterhfs_header;
	fseek(fd, header.hfs0_offset, SEEK_SET);
	fread(&masterhfs_header, 1, sizeof(hfs0_header_t), fd);
	// File position at start of file entry table
	
	hfs0_file_entry_t* part_entries = malloc( sizeof(hfs0_file_entry_t) * masterhfs_header.num_files );
	fread(part_entries, 1, sizeof(hfs0_file_entry_t) * masterhfs_header.num_files, fd);
	// File position at start of stringtable
	
	char* masterhfs_stringtable = malloc(masterhfs_header.stringtable_size);
	fread(masterhfs_stringtable, 1, masterhfs_header.stringtable_size, fd);
	// File position at start of file data area
	
	uint64_t masterhfs_data_start = ftell(fd);
	
	for(int i=0; i<masterhfs_header.num_files; i++) {
		char* part_name = &masterhfs_stringtable[ part_entries[i].file_name_offset ];
		int ret = mkdir(part_name, 0777);
		if(ret != 0)
			bail("Failed to create directory");
		chdir(part_name);
		
		hfs0_header_t part_header;
		fseek(fd, masterhfs_data_start + part_entries[i].file_offset, SEEK_SET);
		fread(&part_header, 1, sizeof(hfs0_header_t), fd);
		
		hfs0_file_entry_t* entries = malloc( sizeof(hfs0_file_entry_t) * part_header.num_files);
		fread(entries, 1, sizeof(hfs0_file_entry_t) * part_header.num_files, fd);
		// File position is now start of stringtable
		
		char* stringtable = malloc(part_header.stringtable_size);
		fread(stringtable, 1, part_header.stringtable_size, fd);
		// File position is now start of file data area
		
		uint64_t part_data_start = ftell(fd);
		
		for(int j=0; j<part_header.num_files; j++) {
			char* file_name = &stringtable[ entries[j].file_name_offset ];
			FILE* outfd = fopen(file_name, "wb");
			
			// Set up for sendfile
			char nullbyte = '\0';
			fseek(outfd, entries[j].file_size - 1, SEEK_SET);
			fwrite(&nullbyte, 1, 1, outfd);
			fseek(outfd, 0, SEEK_SET);
			
			uint64_t writtensize = 0;
			while(writtensize < entries[j].file_size) {
				off_t readoffset = part_data_start + entries[j].file_offset + writtensize;
				writtensize += sendfile( fileno(outfd), fileno(fd), &readoffset, entries[j].file_size - writtensize);
			}
			
			fclose(outfd);
		}
		chdir("..");
		
		free(stringtable);
		free(entries);
	}
	free(masterhfs_stringtable);
	free(part_entries);
	
	fclose(fd);
}
