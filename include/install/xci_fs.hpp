#pragma once

namespace tin::install::xci
{
	class xciFS final
	{
		private:
			FILE * xci;
			u64 secure_start;
			hfs0_header_t header;
			hfs0_file_entry_t * file_table;
			char ** file_names;
		public:
			xciFS(FILE * xci);
			~xciFS();
			char ** getFileNamesFromExtension(char * extension, int * successful_finds);
			int readFile(char * filename, size_t size, u64 offset, void * out);
			int doesFileExist(char * filename);
			int hashFile(char * filename, int patch_gc_byte, void * out); /* patch_gc_byte patches the gc byte in the header to make everything think its an nsp */
	}
}
