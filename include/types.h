#pragma once

#include <stdint.h>

enum {
	TITLE_TYPE_SYSTEM_PROGRAM	= 0x01,
	TITLE_TYPE_SYSTEM_DATA		= 0x02,
	TITLE_TYPE_SYSTEM_UPDATE	= 0x03,
	TITLE_TYPE_FIRMWARE_A 		= 0x04,
	TITLE_TYPE_FIRMWARE_B		= 0x05,
	TITLE_TYPE_APPLICATION		= 0x80,
	TITLE_TYPE_APP_UPDATE		= 0x81,
	TITLE_TYPE_ADDON_CONTENT	= 0x82,
	TITLE_TYPE_DELTA			= 0x83, // No idea what this is supposed to be
};

char* title_type_to_string(uint8_t type);
